# AbobaCrawl

This project contains crawlers and collected datasets for ababa projects.

## How to use this Docker image

---

```shell
docker login registry.gitlab.com
docker run -p 5000:5000 -p 6800:6800 registry.gitlab.com/horasachy1/aboba-crawl
```

This will store the workspace in /crawler/crawler-data.
You will probably want to make that a persistent volume (recommended):

```shell
docker login registry.gitlab.com
docker run -p 5000:5000 -p 6800:6800 -v /your/home:/crawler/crawler-data registry.gitlab.com/horasachy1/aboba-crawl:tag
docker run -p 5000:5000 -p 6800:6800 -v /tmp/crawler-data:/crawler/aboba-crawl/crawler-data aboba-crawl
If exists locally:
```
docker build -t aboba-crawl . 
docker run -p 5000:5000 -p 6800:6800 aboba-crawl:latest 
```
### ScrapydWeb Server
```shell
http://localhost:5000
```
### Scrapyd Server
```shell
http://localhost:6800
```

## How to run spiders
Go to: 
```
http://localhost:5000
```
Select on the left in the menu: 
```
Operations -> Deploy Project
```
```
1. Click Package & Deploy
2. Select spider
3. Click settings & arguments (if need add custome args)
4. Clear additional and enter "-d args_name1=val1,val2 -d args_name2=val1,val2" (if need add custome args)
5. Click Check CMD & Run Spider
```
---
## How to use source code
### Example:
```
cd aboba-crawl/ (Go to crawler directory)
```
```python
scrapy crawl pdf -a urls="https://amco-metall.de/"
```

