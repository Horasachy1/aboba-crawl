from scrapy import Item, Field
from typing import List, Union

def check_text_length(value: Union[str, List[str]], min_length: int = 1) -> Union[str, List[str], None]:
    if isinstance(value, str):
        return value if len(value) > min_length else None
    elif isinstance(value, list):
        text_list: List[str] = []
        for t in value:
            text_list.append(t) if len(t) > min_length else None
        return text_list if len(text_list) > 0 else None


def normalize_space(value) -> str:
    return " ".join(value.split())


class PDFItem(Item):
    file_urls = Field()
    files = Field()
