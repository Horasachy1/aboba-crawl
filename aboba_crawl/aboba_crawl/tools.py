import csv
import re
import logging
import string
from typing import Union, List
from urllib.parse import urlparse


# Get domain name from url.
def get_domain_name(value: Union[str, list]) -> Union[str, list, None]:
    if isinstance(value, str):
        return urlparse(value).netloc
    elif isinstance(value, list):
        return [urlparse(url).netloc for url in value]
    else:
        return None


def get_urls_from_csv_file(csv_file: str) -> List[str]:
    urls: Union[List[str], None] = None
    try:
        with open(csv_file) as file:
            rows = list(csv.reader(file))
            keys = rows.pop(0)
            for row in rows:
                website = {key: value for key, value in zip(keys, row)}
                urls.append(website['website'])
    except Exception as e:
        logging.error(f"Can't load urls {csv_file}. {e}")
    return urls


def get_domains_from_csv_file(csv_file: str) -> list:
    domains: Union[List[str], None] = None
    return [domains.append(get_domain_name(url)) for url in get_urls_from_csv_file(csv_file)]


def remove_emails(value: str) -> str:
    return re.sub(r'\S*@\S*\s?', r'', value)


def remove_phones(value: str) -> str:
    return re.sub(r'[+]\d{2}\W\d+\W\d+\W*\d*', r'', value)


def remove_phone_numbers(value: str) -> str:
    return re.sub(r'[(]\w{3}(\W*\d{2}\W{1}\d{2})+[)]', r'', value)


def remove_urls(value: str) -> str:
    return re.sub(r'http\S+', '', value)


def remove_punctuation(value: str) -> str:
    return value.translate(str.maketrans('', '', string.punctuation))


def remove_multiple_space(value: str) -> str:
    return ' '.join(value.split())


def remove_bad_symbols(value: str) -> str:
    return re.sub(r'[®•·„“»©]', r'', value)


def remove_chinese_letters(value: str) -> str:
    return re.sub(r'[汉语]', r'', value)