import logging
from scrapy.http import Response
from scrapy.loader import ItemLoader
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from typing import AnyStr, Dict, Tuple, List, Union

from aboba_crawl.tools import get_domain_name
from aboba_crawl.items import PDFItem

from aboba_crawl.xpaths import PDFPath


class PDFSpider(CrawlSpider):
    name = 'pdf'
    custom_settings: Dict = {
        'DOWNLOAD_DELAY': 0.1,
        'CONCURRENT_REQUESTS': 1,
        'MAX_CONCURRENT_REQUESTS_PER_DOMAIN': 1,
        'CONCURRENT_REQUESTS_PER_IP': 1,
        'ITEM_PIPELINES': {
            'aboba_crawl.pipelines.DownloadPdfFilesPipeline': 1,
            'aboba_crawl.pipelines.PDFWriterPipeline': 300,
        },
        'FILES_STORE': './crawler-data/items/files'
    }

    # scrapy crawl pdf -a urls="https://www.ibm.com/docs/en/cics-ts/6.1_beta?topic=available-documentation-in-pdf"
    def __init__(self, urls: Union[AnyStr, List, Tuple] = None, *args, **kwargs):
        self.start_urls = urls if isinstance(urls, List) or isinstance(urls, Tuple) else [urls]
        self.allowed_domains: List = [get_domain_name(urls)]
        self.pdf_xpath = PDFPath()
        self.rules: Tuple[Rule] = (
            Rule(LinkExtractor(allow='', deny=r'.*\.pdf$', deny_extensions=('pdf'), allow_domains=self.allowed_domains),
                 follow=False, callback='parse'),
        )
        super(PDFSpider, self).__init__(*args, **kwargs)
        self.logger.setLevel(level=logging.INFO)

    def parse(self, response: Response, **kwargs):
        self.logger.info("Parse pdf page URL: %s", response.url)
        pdf_files = response.xpath(self.pdf_xpath.pdf_link).getall()
        for item in pdf_files:
            try:
                loader: ItemLoader = ItemLoader(item=PDFItem(), selector=item)
                loader.add_value('file_urls', response.urljoin(item))
                yield loader.load_item()
            except IndexError as e:
                self.logger.error(e)
