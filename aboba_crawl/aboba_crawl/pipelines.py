import os
import csv
import json
import glob

from typing import List, Dict

from tika import parser
from scrapy.pipelines.files import FilesPipeline
from aboba_crawl.tools import get_domain_name


class DownloadPdfFilesPipeline(FilesPipeline):
    def file_path(self, request, response=None, info=None, *, item=None):
        file_name: str = request.url.split("/")[-1]
        return file_name


class PDFWriterPipeline:
    def __init__(self):
        self.files_path = 'crawler-data/items/files'

    def process_item(self, item, spider):
        self._read_pdf_data_and_extract(item)
        return item

    def _read_pdf_data_and_extract(self, item) -> None:
        if item.get("files"):
            pdf_file_path: str = os.path.abspath(f"{self.files_path}/{item['files'][0]['path']}")
            raw: Dict = parser.from_file(pdf_file_path)
            pdf_dict: Dict = {
                'all_texts': raw['content'].replace('\n', ''),
                'domain': get_domain_name(item['files'][0]['url']),
                'url': item['files'][0]['url']
            }
            self._extract_to_csv(pdf_dict, item)
            self._extract_csv_to_json(pdf_dict, item)

    def _extract_to_csv(self, pdf_dict: Dict, item) -> None:
        filename: str = f"{self.files_path}/{item['files'][0]['path'].split('.pdf')[0]}.csv"
        if not filename in self.get_files_by_expansion(self.files_path, 'csv'):
            with open(filename, 'w') as f:
                w: csv.DictWriter = csv.DictWriter(f, pdf_dict.keys())
                w.writeheader()
                w.writerow(pdf_dict)

    def _extract_csv_to_json(self, pdf_dict: Dict, item) -> None:
        filename: str = f"{self.files_path}/{item['files'][0]['path'].split('.pdf')[0]}.json"
        if not filename in self.get_files_by_expansion(self.files_path, 'json'):
            with open(filename, 'w') as jsonfile:
                json.dump(pdf_dict, jsonfile)

    @staticmethod
    def get_files_by_expansion(path: str, extension: str) -> List:
        return glob.glob(os.path.join(path, f"*.{extension}"))
